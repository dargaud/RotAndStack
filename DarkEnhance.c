#include <cvirte.h>		
#include <userint.h>
#include "DarkEnhance.h"

static int Pnl;

int main (int argc, char *argv[])
{
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "DarkEnhance.uir", PNL)) < 0)
		return -1;
	DisplayPanel (Pnl);
	RunUserInterface ();
	DiscardPanel (Pnl);
	return 0;
}

int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2)
{
	switch (event)
		{
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
		case EVENT_PANEL_SIZE:

			break;
		}
	return 0;
}

int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
		{
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		}
	return 0;
}

int CVICALLBACK cb_Resize (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2)
{
	switch (event)
		{
		case EVENT_COMMIT:

			break;
		}
	return 0;
}
