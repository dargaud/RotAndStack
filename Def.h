/**************************************************************************************************
	FILE:       def.h

	PURPOSE:    General declarations and macros. Used by both MSVC and LabWindows/CVI
	NOTE:       The macros _NI_mswin_ and _MSC_VER controls whether to compile for CVI or MSVC
	SVN:        $Id: Def.h 2066 2009-05-26 16:12:11Z /C=FR/O=CNRS/OU=UMR5821/CN=Guillaume Dargaud/emailAddress=dargaud@lpsc.in2p3.fr $
**************************************************************************************************/

#ifndef _DEF
#define _DEF

#include <iso646.h>	// for 'and', 'or'...

#ifndef NULL
#define NULL ((void*)0)
#endif

///////////////////////////////////////////////////////////////////////////////
// True IEEE Nan and Inf handling
#define dNaN  ((unsigned __int64)0xFFF8000000000000)
#define dpInf ((unsigned __int64)0xFFF0000000000000)
#define dnInf ((unsigned __int64)0x7FF0000000000000)
#define IsDoubleNaN(d)      (*(unsigned __int64*)&(d)==dNaN)
#define IsDoublePlusInf(d)  (*(unsigned __int64*)&(d)==dpInf)
#define IsDoubleMinusInf(d) (*(unsigned __int64*)&(d)==dnInf)
#define IsDoubleInf(d) (IsDoublePlusInf(d) or IsDoubleMinusInf(d))
#define IsInvalid(d) (IsDoubleNaN(d) or IsDoublePlusInf(d) or IsDoubleMinusInf(d))


// Swap 32 bit endian value
#ifndef SWAP_ENDIAN
#define SWAP_ENDIAN(a) ( (((a) bitand 0x000000FF) << 24) bitor\
						 (((a) bitand 0x0000FF00) <<  8) bitor\
						 (((a) bitand 0x00FF0000) >>  8) bitor\
						 (((a) bitand 0xFF000000) >> 24) )
#endif

///////////////////////////////////////////////////////////////////////////////
// Some are redefined (differently) in toolbox.h
//#define Pi       3.1415926535897932384626433832795028841971L
#define RAD(x)   ((x)*PI/180.0)
// Normalized sin(x)/x
#define SINC(x)  ((x) != 0.0  ? sin(PI*(x))/(PI*(x)) : 1.0)
//#define Ln10     2.3025850929940456840179914546844L    // For inverse of log10. Use 10^x==exp(x*Ln10)
#define Exp10(x) exp((x)*Ln10)
// Use * for conversions between degrees and radians
//#define DegToRad 0.017453292519943295769236907684886L 
//#define RadToDeg 57.295779513082320876798154814105L   

///////////////////////////////////////////////////////////////////////////////
// Time conversion
#define MS2CVI_TIME   2208902400   // Because MS time starts at 1/1/1970 and ANSI C at 1/1/1900
//#define MS2CVI_TIME 2208978000   // Because MS time starts at 1/1/1970 and ANSI C at 1/1/1900
#define IsCviTime(t) ((t)>=MS2CVI_TIME)	// Check if a date after 1970 is in CVI format
#define XL2CVI_TIME(XL) (((XL) - 25569) * 86400)	// XL is a double, result is time_t
#define CVI2XL_TIME(t) ((t)/86400. + 25569)			// t is time_t, result is a double


///////////////////////////////////////////////////////////////////////////////
#define Malloc(type)   (type *)malloc(sizeof(type))
#define Calloc(n,type) (type *)calloc(n, sizeof(type))
#define Free(Ptr) { free(Ptr); Ptr=NULL; }

///////////////////////////////////////////////////////////////////////////////
#define MIN(a,b)    ((a)<=(b) ? (a):(b))
#define MAX(a,b)    ((a)>=(b) ? (a):(b))
#define MIN3(a,b,c) ((a)<=(b) ? (a)<=(c)?(a):(c) : (b)<=(c)?(b):(c) )
#define MAX3(a,b,c) ((a)>=(b) ? (a)>=(c)?(a):(c) : (b)>=(c)?(b):(c) )
#define BETWEEN(a,b,c) ((a)<=(b) and (b)<=(c))
#define FORCEPIN(a,b,c) ((b)<=(a) ? (a) : (c)<=(b) ? (c) : (b))	// Forces b to be between a and c
#define SIGN(a) ((a)>0 ? 1 : (a)<0 ? -1 : 0)

///////////////////////////////////////////////////////////////////////////////
// Log of 2 - related macros
#define InvLn2 1.4426950408889634073599246810019L       // 1/ln(2)
#define FloorLog2(x) ((int)(floor(log(x)*InvLn2)))
#define RoundLog2(x) ((int)(floor(log(x)*InvLn2+0.5)))
#define CeilLog2(x)  ((int)(ceil( log(x)*InvLn2)))

///////////////////////////////////////////////////////////////////////////////
typedef struct {
	float Re, Im;
} complex;

//typedef enum {false, true} bool;

// Basic types (from Windows)
//#pragma warning(disable:4209 4142)

typedef unsigned char  BYTE;        // 8-bit unsigned entity
typedef unsigned short WORD;        // 16-bit unsigned number
typedef unsigned int   UINT;        // machine sized unsigned number (preferred)
//typedef long           LONG;      // 32-bit signed number
typedef unsigned long  DWORD;       // 32-bit unsigned number
typedef void*          POSITION;    // abstract iteration position

#ifndef BOOL
#define BOOL int
#endif

#ifdef _CVI_
#pragma iso_9899_1999		// Keep __FUNCTION__ as a macro
#endif

#if _CVI_ < 810 	// Panel sizing event work only in recent versions
#define EVENT_PANEL_SIZING         37  /* eventData1 = sizing edge (PANEL_SIZING_TOPLEFT, etc) */
#define EVENT_MOUSE_WHEEL_SCROLL   36
#define MOUSE_WHEEL_PAGE_UP         0
#define MOUSE_WHEEL_PAGE_DOWN       1
#define MOUSE_WHEEL_SCROLL_UP       2
#define MOUSE_WHEEL_SCROLL_DOWN     3
#endif


#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif
#ifndef YES
#define YES 1
#define NO 0
#endif
#ifndef OPEN
#define OPEN 1
#define CLOSE 0
#endif
#ifndef MODIF
#define MODIF 1
#define NOMODIF 0
#endif

///////////////////////////////////////////////////////////////////////////////
//#pragma warning(default:4209 4142)

#ifdef _DEBUG
	// NOTE: you should #include <stdlib.h> if you use those macros
	#define TRACE     printf
	#define ASSERT(f) ((f) ? (void)0 : printf("Assertion " #f " failed file %s line %d",__FILE__, __LINE__))
#else
	#define ASSERT(f) ((void)0)
	#define TRACE     ((void)0)
#endif // _DEBUG


#endif // _DEF
