PROGRAMS:   RotAndStack.exe

VERSION:    1.2

AUTHOR:     Guillaume Dargaud                                 

PURPOSE:    Generation of color palettes based on RGB or HSL interpolation between given colors.

INSTALL:    run SETUP.  
            If setup fails, you may need to install the LabWindows/CVI runtime engine on your PC.  
            It is available from the National Instrument website http://www.ni.com/

HELP:       available by right-clicking an item on the user interface.

TUTORIAL:   http://www.gdargaud.net/Hack/RotAndStack.html

LICENSE:   GPLv2
