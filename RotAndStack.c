/////////////////////////////////////////////////////////////////////////////////////
// PROGRAM		GenPalette.exe
// COPYRIGHT	Guillaume Dargaud 1999-2012 - Freeware
// PURPOSE		Create palette files
// INSTALL		run SETUP. It will also install the LabWindows/CVI runtime engine.
// HELP			available by right-clicking an item.
// TUTORIAL		http://www.gdargaud.net/Hack/GenPalette.html
/////////////////////////////////////////////////////////////////////////////////////

#define _VER_ "1.2"			// Version number
#define _WEB_ "http://www.gdargaud.net/Hack/RotAndStack.html"
#define _COPY_ "(c) 2005-2012 Guillaume Dargaud"

char HELP[]="Rot'n'Stack is a basic astrophography freeware.\n"
			"It takes a bunch of image files (jpg or png) of same size.\n"
			"It asks you for two reference stars on the first two images,\n"
			"then it searches for the same stars on the other images.\n"
			"Use [Animate] or the up/down arrow keys to navigate images.\n"
			"After you confirm the location of those stars you can\n"
			"launch the rotation process, where all the images are aligned\n"
			"and launch the stacking process where the images are merged\n"
			"using various algorithms.\n"
			"Intermediate (optional) and final images are saved into the same directory\n"
			"as your original images.\n\n"
			"[Right-click] on a control for more help.\n\n"
			"Rot'n'Stack " _VER_ " " _COPY_ "\n"
			"Tutorial at " _WEB_ "\nLast compiled on " __DATE__;
			

#include <ansi_c.h>
#include <utility.h>
#include <cvirte.h>		
#include <userint.h>

#include <iso646.h>
#include <Def.h>

#include "toolbox.h"
#include "ReadSaveJpeg.h"
#include "ReadSavePng.h"

#include "RotAndStack.h"

static char DriveName[MAX_DRIVENAME_LEN]="", DirName[MAX_DIRNAME_LEN]="",  Directory[MAX_PATHNAME_LEN]="";
static char **FileList=NULL, **DestFileList=NULL;
static int NbFiles=0, Current=-1;
static BOOL Dropped=FALSE;	// indicate if the files were dropped or opened

// Array of bit arrays
static int Bitmap=0;
static unsigned char *Bits=NULL, *DestBits=NULL, *DarkBits=NULL;
static int RowBytes=0, PixDepth=0, Width=0, Height=0;	// Common to all files
static int DestRowBytes=0, DestWidth=0, DestHeight=0;	// Destination maximizes rotations (all images are 'in')
static int DestOffsetMinX=0, DestOffsetMinY=0, DestOffsetMaxX=0, DestOffsetMaxY=0;
static char Str[1000];

static int Pnl=0;
static int SearchWidth=10, Blur=2;	// 2 pixels on each side, (2*Blur+1)^2=25 pixels total
static BOOL Negative=FALSE;			// Look for light or dark points

static int CanvasHeight=0, CanvasWidth=0, CanvasLeft=0, CanvasTop=0;

static BOOL Transformed=FALSE, SaveRot=FALSE;

static int ColorLeft=VAL_YELLOW; ColorRight=VAL_CYAN;
static int  *XLeft=NULL, *XRight=NULL, *YLeft=NULL, *YRight=NULL, *Within=NULL;
static double *Alpha=NULL, *Rad=NULL;
static int *XoffsetMin, *YoffsetMin, *XoffsetMax, *YoffsetMax;
static BOOL *ManualCrossL=NULL, *ManualCrossR=NULL, Static=TRUE;;

static char LeftPathName[MAX_PATHNAME_LEN]="", RightPathName[MAX_PATHNAME_LEN]="",
			StatFilePath[MAX_PATHNAME_LEN]="", 
			RGBMeanFilePath[MAX_PATHNAME_LEN]="", RGBMaxFilePath[MAX_PATHNAME_LEN]="", 
			RGBMinFilePath[MAX_PATHNAME_LEN]="",  RGBSortFilePath[MAX_PATHNAME_LEN]="", 
			HSLMeanFilePath[MAX_PATHNAME_LEN]="", HSLMaxFilePath[MAX_PATHNAME_LEN]="";

static FILE *StatFile=NULL;

static int *StackSum=NULL,	// Rowbytes*Height*4 (one per channel)
			*StackNb=NULL;	// Rowbytes*Height
static unsigned char *StackMax=NULL, *StackMin=NULL, *StackMean=NULL, *StackSort=NULL;	// Rowbytes*Height

static double Gamma=2;
static double ThresholdLow=4, ThresholdHigh=96;

static double SumR=0, SumG=0, SumB=0, SumA=0,
			  SSmR=0, SSmG=0, SSmB=0, SSmA=0;
static unsigned long CntR=0, CntG=0, CntB=0, CntA=0;

static int LastRowBytes=0, LastPixDepth=0, LastWidth=0, LastHeight=0;

static BOOL AutoDark=FALSE;					// Look for light or dark points
static char DarkFile[MAX_PATHNAME_LEN]="";	// Not used if empty


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "RotAndStack.uir", PNL)) < 0)
		return -1;
	
	SetStdioPort (CVI_STDIO_WINDOW);
	SetStdioWindowOptions (1000, FALSE, FALSE);
	SetStdioWindowVisibility (FALSE);
	
	EnableDragAndDrop(Pnl);
	DisplayPanel(Pnl);
	cbp_Panel (Pnl, EVENT_PANEL_SIZE, NULL, 0, 0);
	GetCtrlVal(Pnl, PNL_SEARCH,        &SearchWidth);
	GetCtrlVal(Pnl, PNL_BLUR,          &Blur);
	GetCtrlVal(Pnl, PNL_GAMMA,         &Gamma);
	GetCtrlVal(Pnl, PNL_THRESHOLD_LOW, &ThresholdLow);
	GetCtrlVal(Pnl, PNL_THRESHOLD_HIGH,&ThresholdHigh);
	GetCtrlVal(Pnl, PNL_NEGATIVE,      &Negative);
	GetCtrlVal(Pnl, PNL_SAVE_ROT,      &SaveRot);
	GetCtrlVal(Pnl, PNL_COLOR_LEFT,    &ColorLeft);
	GetCtrlVal(Pnl, PNL_COLOR_RIGHT,   &ColorRight);
				
	#ifndef _CVI_DEBUG_
	MessagePopup("Rot'n'Stack Help", HELP);
	#endif
	RunUserInterface ();
	
	DiscardPanel (Pnl);
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// Identification is based only on the file extension
static BOOL IsJpeg(const char* Name) {
	int L=strlen(Name);
	return 
		(L>=4 and toupper(Name[L-4])=='.' and toupper(Name[L-3])=='J' and toupper(Name[L-2])=='P' and toupper(Name[L-1])=='G') or
		(L>=5 and toupper(Name[L-5])=='.' and toupper(Name[L-4])=='J' and toupper(Name[L-3])=='P' and toupper(Name[L-2])=='E' and toupper(Name[L-1])=='G');
}

static BOOL IsPng(const char* Name) {
	int L=strlen(Name);
	return (L>=4 and toupper(Name[L-4])=='.' and toupper(Name[L-3])=='P' and toupper(Name[L-2])=='N' and toupper(Name[L-1])=='G');
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Draw a cross on the canvas at a given coord
/// HIPAR	X/in image coordinate system (not canvas position)
/// HIPAR	Color/of the cross
/// HIPAR	Diagonal/TRUE for a diagonal cross, straight otherwise
///////////////////////////////////////////////////////////////////////////////
static void CanvasDrawCross(int X, int Y, int Color, BOOL Diagonal) {
	int Xc=X*CanvasWidth/Width, Yc=Y*CanvasHeight/Height, S=3, 
		H=SearchWidth*CanvasHeight/Height, W=SearchWidth*CanvasWidth/Width;

	SetCtrlAttribute (Pnl, PNL_CANVAS, ATTR_PEN_COLOR, Color);
	SetCtrlAttribute (Pnl, PNL_CANVAS, ATTR_PEN_MODE, VAL_COPY_MODE);
	SetCtrlAttribute (Pnl, PNL_CANVAS, ATTR_PEN_WIDTH, 1);
	SetCtrlAttribute (Pnl, PNL_CANVAS, ATTR_PEN_STYLE, VAL_SOLID);

	if (Diagonal) {
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc-S, Yc-S), MakePoint(Xc-H, Yc-H));
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc+S, Yc+S), MakePoint(Xc+H, Yc+H));
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc-S, Yc+S), MakePoint(Xc-W, Yc+W));
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc+S, Yc-S), MakePoint(Xc+W, Yc-W));
	} else {
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc, Yc-S), MakePoint(Xc, Yc-H));
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc, Yc+S), MakePoint(Xc, Yc+H));
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc-S, Yc), MakePoint(Xc-W, Yc));
		CanvasDrawLine (Pnl, PNL_CANVAS, MakePoint(Xc+S, Yc), MakePoint(Xc+W, Yc));
	}
}



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read image i and put it into bitmap control and Bits array
///////////////////////////////////////////////////////////////////////////////
static void ReadFile(int i) {
	static int LastRead=-1;
	
	if (LastRead==i) return;
	LastRead=i;
	if (Bitmap!=0) DiscardBitmap (Bitmap); Bitmap=0;
	if (Bits!=NULL) free(Bits); Bits=NULL;
	
	if (IsJpeg(FileList[i])) {
		if (Jpg_ReadBitmapFromFile (&Bitmap, &Bits, &RowBytes, &PixDepth, &Width, &Height, FileList[i])) {
			sprintf(Str, "Error reading file %s", FileList[i]);
			MessagePopup("Error!", Str);
			return;
		}
	} else if (IsPng(FileList[i])) {
		if (Png_ReadBitmapFromFile (&Bitmap, &Bits, &RowBytes, &PixDepth, &Width, &Height, FileList[i])) {
			sprintf(Str, "Error reading file %s", FileList[i]);
			MessagePopup("Error!", Str);
			return;
		}
	} else {
		sprintf(Str, "File %s is not png or jpg", FileList[i]);
		MessagePopup("Error!", Str);
		return;
	}

	// Check that size is always the same
	if (LastRowBytes==0) {
		LastRowBytes=RowBytes; LastPixDepth=PixDepth; LastWidth=Width; LastHeight=Height;
	} else if (LastRowBytes!=RowBytes or LastPixDepth!=PixDepth or LastWidth!=Width or LastHeight!=Height)
		MessagePopup("Error !", "All the images MUST have the same dimensions.");
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Display image i on canvas
/// HIPAR	i/from 0 to NbFiles
///////////////////////////////////////////////////////////////////////////////
static void DisplayImage(int i) {
	SetCtrlAttribute(Pnl, PNL_FOLLOW, ATTR_DIMMED,
		NbFiles<2 or Current<1 or Current==NbFiles-1 or
		XLeft[Current  ]==0 or XRight[Current  ]==0 or
		XLeft[Current-1]==0 or XRight[Current-1]==0);
	
	if (i<0 or i>=NbFiles) {
		ClearListCtrl    (Pnl, PNL_IMAGE_LIST);
		CanvasClear      (Pnl, PNL_CANVAS, VAL_ENTIRE_OBJECT);
		SetPanelAttribute(Pnl, ATTR_TITLE, "Rot'n'Stack");
	} else {
		char Label[MAX_FILENAME_LEN]="";
		SetCtrlIndex (Pnl, PNL_IMAGE_LIST, i);	// Current is not necessarily the same as the image we want to display
		ReadFile(i);
		CanvasDrawBitmap (Pnl, PNL_CANVAS,/*Transformed ? DestBitmap[i] :*/ Bitmap, VAL_ENTIRE_OBJECT, VAL_ENTIRE_OBJECT);
		if (XLeft[i]!=0 ) CanvasDrawCross(/*Transformed ? XLeft[0] :*/ XLeft[i],  /*Transformed ? YLeft[0] :*/ YLeft[i],  ColorLeft,  !ManualCrossL[i]);
		if (XRight[i]!=0) CanvasDrawCross(/*Transformed ? XRight[0]:*/ XRight[i], /*Transformed ? YRight[0]:*/ YRight[i], ColorRight, !ManualCrossR[i]);
		GetLabelFromIndex (Pnl, PNL_IMAGE_LIST, Current, Label);
		sprintf(Str, "Rot'n'Stack - %s - %dx%d - Zoom %.0f%%", 
			Label, Width, Height,
			100/sqrt(Width*Width+Height*Height)*sqrt(CanvasWidth*CanvasWidth+CanvasHeight*CanvasHeight));
		SetPanelAttribute (Pnl, ATTR_TITLE, Str);
	}		
	CanvasUpdate (Pnl, PNL_CANVAS, VAL_ENTIRE_OBJECT);
	ProcessDrawEvents ();
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Free memory
///////////////////////////////////////////////////////////////////////////////
static void Cleanup(void) {
	int i;
	
	#define FREE(P)	if (P!=NULL) free(P); P=NULL;

	if (FileList!=NULL) {
		for (i=0; i<NbFiles; i++) {
			free(FileList[i]);
			free(DestFileList[i]);
		}
		if (Bitmap!=0) DiscardBitmap(Bitmap); Bitmap=0;
		FREE(Bits);
		FREE(DarkBits);
		FREE(FileList);
		FREE(DestFileList);
		FREE(DestBits);
		FREE(XLeft );
		FREE(XRight);
		FREE(YLeft );
		FREE(YRight);
		FREE(Rad);	
		FREE(Alpha);
		FREE(XoffsetMin);
		FREE(YoffsetMin);
		FREE(XoffsetMax);
		FREE(YoffsetMax);
		FREE(Within);	
		FREE(StackSum);	
		FREE(StackNb);	
		FREE(StackMax);	
		FREE(StackMin);	
		FREE(StackMean);
		FREE(ManualCrossL);
		FREE(ManualCrossR);
	}
	ClearListCtrl(Pnl, PNL_IMAGE_LIST);
	// SetCtrlAttribute(Pnl, PNL_STACK, ATTR_VISIBLE, FALSE);
	Transformed=FALSE;
	LastRowBytes=LastPixDepth=LastWidth=LastHeight=0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Allocate necessary memory
///////////////////////////////////////////////////////////////////////////////
static void Define(void) {
	int i;
	char FileName[MAX_FILENAME_LEN]="";

	// Array of bit arrays
	DestFileList	= calloc(NbFiles, sizeof(char*));
	Bitmap			= 0;
	// DestBitmap		= calloc(NbFiles, sizeof(int));
	XLeft			= calloc(NbFiles, sizeof(int));
	YLeft			= calloc(NbFiles, sizeof(int));
	XRight			= calloc(NbFiles, sizeof(int));
	YRight			= calloc(NbFiles, sizeof(int));
	Rad				= calloc(NbFiles, sizeof(double)); for(i=0;i<NbFiles;i++) Rad[i]=1.;
	Alpha			= calloc(NbFiles, sizeof(double));
	XoffsetMin		= calloc(NbFiles, sizeof(int));
	YoffsetMin		= calloc(NbFiles, sizeof(int));
	XoffsetMax		= calloc(NbFiles, sizeof(int));
	YoffsetMax		= calloc(NbFiles, sizeof(int));
	Within			= calloc(NbFiles, sizeof(int));
	ManualCrossL	= calloc(NbFiles, sizeof(BOOL));
	ManualCrossR	= calloc(NbFiles, sizeof(BOOL));
	
	if (DestFileList==NULL or XLeft==NULL or YLeft==NULL or XRight==NULL or YRight==NULL or 
		Rad==NULL or Alpha==NULL or Within==NULL or ManualCrossL==NULL or ManualCrossR==NULL or
		XoffsetMin==NULL or YoffsetMin==NULL or XoffsetMax==NULL or YoffsetMax==NULL) {
		MessagePopup("Error", "Out of memory error, increase pagefile size.");
		exit(2);
	}
	
	for (i=0; i<NbFiles; i++) {
		SplitPath (FileList[i], DriveName, DirName, FileName);
		InsertListItem (Pnl, PNL_IMAGE_LIST, -1, FileName, i);
	}

	ReadFile(Current=0);
	cb_11(Pnl, PNL_11, EVENT_COMMIT, NULL, 0, 0);
	DisplayImage(0); 

	MessagePopup("Select References", "Now select two reference stars on the 1st image\n"
									"with the left and right click for each.");
}


///////////////////////////////////////////////////////////////////////////////
// Compares two strings
static int STRCMP(const void *element1, const void *element2) {
	return strcmp(*(char**)element1, *(char**)element2);
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int W, H;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute(panel, ATTR_WIDTH, &W);
			GetPanelAttribute(panel, ATTR_HEIGHT, &H);
			SetCtrlAttribute(panel, PNL_CANVAS, ATTR_WIDTH, CanvasWidth=W);
			GetCtrlAttribute(panel, PNL_CANVAS, ATTR_TOP,  &CanvasTop);
			GetCtrlAttribute(panel, PNL_CANVAS, ATTR_LEFT, &CanvasLeft);
			SetCtrlAttribute(panel, PNL_CANVAS, ATTR_HEIGHT,CanvasHeight=H-CanvasTop);
			DisplayImage(Current);
			break;

		case EVENT_KEYPRESS:
		case EVENT_MOUSE_WHEEL_SCROLL:
			switch (eventData1) {
				case VAL_DOWN_ARROW_VKEY:
				case MOUSE_WHEEL_SCROLL_DOWN:
					SetWaitCursor(TRUE);
					SetCtrlVal(Pnl, PNL_IMAGE_LIST, Current>=NbFiles-1 ? Current=0 : ++Current);
					SetActiveCtrl(Pnl, PNL_IMAGE_LIST); 
					DisplayImage(Current);
					SetWaitCursor(FALSE);
					return 1;

				case VAL_UP_ARROW_VKEY:
				case MOUSE_WHEEL_SCROLL_UP:
					SetWaitCursor(TRUE);
					SetCtrlVal(Pnl, PNL_IMAGE_LIST, Current<=0 ? Current=NbFiles-1 : --Current);
					SetActiveCtrl(Pnl, PNL_IMAGE_LIST); 
					DisplayImage(Current);
					SetWaitCursor(FALSE);
					return 1;
			}			
			break;
			
		case EVENT_FILESDROPPED:
			SetWaitCursor(TRUE);
			Cleanup();
			Dropped=TRUE;
			FileList=(char**)eventData1;
			NbFiles=0;
			while (((char**)eventData1)[NbFiles++]!=NULL);
			NbFiles--;
//			FileList = calloc (NbFiles, sizeof(char*));
//			for (i=0; i<NbFiles; i++) {
//				FileList[i]=malloc(MAX_PATHNAME_LEN);	//strlen(((char**)eventData1)[i])+1);
//				strcpy(FileList[i], ((char**)eventData1)[i]);
//			}
			qsort (FileList, NbFiles, sizeof(char*), /*(int(const void*, const void*))strcmp*/  STRCMP);
			Define();
			SetWaitCursor(FALSE);
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Remove the dark from the frame
///////////////////////////////////////////////////////////////////////////////
void SubstractDark(void) {
	int x, y;
	unsigned char *Col, *Dark;
	if (Bits==NULL or DarkBits==NULL) return;

	for (y=0; y<Height; y++) {
		Col =&Bits    [y*RowBytes];
		Dark=&DarkBits[y*RowBytes];
		for (x=0; x<Width; x++) {
			if (                Dark[0]>Col[0]) Col[0]=0; else Col[0]-=Dark[0];
			if (PixDepth>8  and Dark[1]>Col[1]) Col[1]=0; else Col[1]-=Dark[1];
			if (PixDepth>16 and Dark[2]>Col[2]) Col[2]=0; else Col[2]-=Dark[2];
			if (PixDepth>24 and Dark[3]>Col[3]) Col[3]=0; else Col[3]-=Dark[3];
			Col += (PixDepth>>3);
			Dark+= (PixDepth>>3);	// Keep only the darkest pixel of the stack
		}
	}
}


///////////////////////////////////////////////////////////////////////////////
#define  POS(X, Y) ( (X)*(PixDepth>>3) + (Y)*RowBytes )
#define DPOS(X, Y) ( (X)*(PixDepth>>3) + (Y)*DestRowBytes )
#define AVG_COLOR(P) (PixDepth==8 ) ? ( Negative ? 255-Bits[P] : Bits[P] ) :\
			 		 (PixDepth==24) ? ( Negative ? 255-((double)Bits[P]+Bits[P+1]+Bits[P+2])/3 :           ((double)Bits[P]+Bits[P+1]+Bits[P+2])/3 ) : \
			 		 (PixDepth==32) ? ( Negative ? 255-((double)Bits[P]-Bits[P+1]-Bits[P+2]-Bits[P+3])/4 : ((double)Bits[P]+Bits[P+1]+Bits[P+2]+Bits[P+3])/4 ) :\
					 0

static const int Method=1;	// 1 or 2

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Search for brightest point centered on X/Y, on current image on a window width of SearchWidth
///////////////////////////////////////////////////////////////////////////////
static void FindCenter(int X, int Y, int *Xc, int *Yc) {
	int i, j, P;
	int ii, jj;	// Max value
	double CC, M=0, Gamma=10, D, Cnt=0, Xs=0, Ys=0, C, Cmax=1;
	
	switch (Method) {
	case 1:// Vectorial Barycentric
		for (    i=Max(0,X-SearchWidth); i<=Min(Width-1, X+SearchWidth); i++)
			for (j=Max(0,Y-SearchWidth); j<=Min(Height-1,Y+SearchWidth); j++) {
				P=POS(i,j);
				C=AVG_COLOR(P);
				if (C>Cmax) Cmax=C;
			}
	Retry:
		Cnt=0;
//		DebugPrintf ("\n\nX\tY\tC\tD\tCnt\tXs/Cnt\tYs/Cnt");
		for (    i=Max(0,X-SearchWidth); i<=Min(Width-1, X+SearchWidth); i++)
			for (j=Max(0,Y-SearchWidth); j<=Min(Height-1,Y+SearchWidth); j++) {
				P=POS(i,j);
				C=AVG_COLOR(P);
				D=pow(C/Cmax, Gamma);	// Otherwise not distinctive enough
				Cnt+=D;
				Xs+=D*i;
				Ys+=D*j;
//				DebugPrintf ("\n%d\t%d\t%f\t%f\t%f\t%f\t%f", X, Y, C, D, Cnt, Xs/Cnt, Ys/Cnt);
			}
		if (Cnt==0)
			if (Gamma>1) { Gamma-=0.25; goto Retry; }
			else { *Xc=X;      *Yc=Y; }
		else {	   *Xc=Xs/Cnt; *Yc=Ys/Cnt; }	// Barycenter
		break;

	case 2:// Blurred gaussian Max
	//	int **V=calloc((SearchWidth*2+1)*(SearchWidth*2+1), sizeof(int));	// Array to compute the values
	
		for (    i=Max(0,X-SearchWidth-Blur); i<=Min(Width-1 ,X+SearchWidth+Blur); i++)
			for (j=Max(0,Y-SearchWidth-Blur); j<=Min(Height-1,Y+SearchWidth+Blur); j++) {
				CC=0; Cnt=0;
				for (ii=-Blur; ii<=Blur; ii++)
					for (jj=-Blur; jj<=Blur; jj++) {
						P=POS(i,j);
						C=AVG_COLOR(P);
						CC+=C;
					}
				if (Cnt>0 and CC/Cnt>M) { M=CC/Cnt, *Xc=i; *Yc=j; }
			}
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Loads each image and estimates the position of the moved peaks
/// HIFN	Assumes that X/Y Left/Right 0/1 are set properly
/// HIPAR	From/Starting image and the previous one. 1 by default
///////////////////////////////////////////////////////////////////////////////
static void Follow(int From) {
	int Progress;
	if (From<1 or XLeft[From]==0 or XRight[From]==0 or XLeft[From-1]==0 or XRight[From-1]==0) return;
	
	Progress = CreateProgressDialog ("Finding alignment points", "Percent Complete", 1, VAL_FULL_MARKERS, "__Cancel");

	for (Current=From+1; Current<NbFiles; Current++) {
		UpdateProgressDialog (Progress, 100*Current/NbFiles, 1);
		ReadFile(Current);
		FindCenter( XLeft[Current-1]*2- XLeft[Current-2],  YLeft[Current-1]*2- YLeft[Current-2],  &XLeft[Current],  &YLeft[Current]);
		if (!BETWEEN(1, XLeft[Current], Width-1))  MessagePopup("Error !", "1st reference point out of horizontal field");
		if (!BETWEEN(1, YLeft[Current], Height-1)) MessagePopup("Error !", "1st reference point out of vertical field");

		FindCenter(XRight[Current-1]*2-XRight[Current-2], YRight[Current-1]*2-YRight[Current-2], &XRight[Current], &YRight[Current]);
		if (!BETWEEN(1, XRight[Current], Width-1))  MessagePopup("Error !", "2nd reference point out of horizontal field");
		if (!BETWEEN(1, YRight[Current], Height-1)) MessagePopup("Error !", "2nd reference point out of vertical field");
	}
	
	DiscardProgressDialog (Progress);

	SetCtrlVal   (Pnl, PNL_ANIMATE, TRUE);
	SetActiveCtrl(Pnl, PNL_ANIMATE);
	MessagePopup("Images aligned", "The images have been aligned.\n"
					"Check the results with the [Animate] button.\n"
					"Then press [Stop] and if necessary adjust the position\n"
					"of the a alignement points with left and right click on each image.\n"
					"Manual points have up-down/left-right crosses\n"
					"while automatic points have diagonal crosses.\n"
					"When finished, press [Rot'n'Stack] to perform the rotation and the stacking\n"
					"and to save the resulting images to the same directory.");
	cb_Animate   (Pnl, PNL_ANIMATE, EVENT_COMMIT, NULL, 0, 0);
}


///////////////////////////////////////////////////////////////////////////////
#define Xrot(x,y)  (XLeft[i] + CA*((x)-XLeft[0]) - SA*((y)-YLeft[0]))	// Matrix rotation, zooming and translation
#define Yrot(x,y)  (YLeft[i] + SA*((x)-XLeft[0]) + CA*((y)-YLeft[0]))
#define iXrot(x,y) (XLeft[0] + CA*((x)-XLeft[i]) + SA*((y)-YLeft[i]))	// Inverse rotation
#define iYrot(x,y) (YLeft[0] - SA*((x)-XLeft[i]) + CA*((y)-YLeft[i]))


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Compute the transform coeficients
/// HIPAR	i/The image index
///////////////////////////////////////////////////////////////////////////////
static void ComputeTransform(int i) {
	int XUL, XUR, XLR, XLL,
		YUL, YUR, YLR, YLL;
	double Z, Angle, CA, SA;
	
	Static=TRUE;
	
	Rad[i]  =sqrt( (XRight[i]-XLeft[i])*(XRight[i]-XLeft[i]) + 
		           (YRight[i]-YLeft[i])*(YRight[i]-YLeft[i]) );
	Alpha[i]=atan2( YRight[i]-YLeft[i],  XRight[i]-XLeft[i]);
	
	Z=Rad[0]==0 ? 1 : Rad[i]/Rad[0];
	Angle=Alpha[i]-Alpha[0];
	CA=Z*cos(Angle); SA=Z*sin(Angle);
	if (fabs(Angle)>1e-6) Static=FALSE;	// Detects if at least one of the images is rotated
	
	// Compute the destination of the 4 image corners
	XUL = iXrot(0,0);	// Matrix rotation, zooming and translation
	YUL = iYrot(0,0);
	XUR = iXrot(Width-1,0);
	YUR = iYrot(Width-1,0);
	XLR = iXrot(Width-1,Height-1);
	YLR = iYrot(Width-1,Height-1);
	XLL = iXrot(0,Height-1);
	YLL = iYrot(0,Height-1);
	XoffsetMax[i]=MAX(MAX(XUL, XUR), MAX(XLR, XLL));
	XoffsetMin[i]=MIN(MIN(XUL, XUR), MIN(XLR, XLL));
	YoffsetMax[i]=MAX(MAX(YUL, YUR), MAX(YLR, YLL));
	YoffsetMin[i]=MIN(MIN(YUL, YUR), MIN(YLR, YLL));
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Perform the transform
/// HIPAR	i/The image index
///////////////////////////////////////////////////////////////////////////////
static void PerformTransform(int i) {
	int xd, yd, Xn, Yn;
	int P, D, Pn;
	double Z, Angle, CA, SA;

	if (DestBits!=NULL) free(DestBits); DestBits=NULL;
	DestBits=malloc(DestRowBytes*DestHeight);
	if (DestBits==NULL) MessagePopup("Error", "Out of memory error, increase pagefile size.");
	ReadFile(i);
	
	if (AutoDark or DarkFile[0]!='\0') SubstractDark();
	
	Z=Rad[0]==0 ? 1 : Rad[i]/Rad[0];
	Angle=Alpha[i]-Alpha[0];
	CA=Z*cos(Angle); SA=Z*sin(Angle);

	for (yd=0; yd<DestHeight; yd++)		// This is the destination coordinates
		for (xd=0; xd<DestWidth; xd++) {
			P=POS(xd-DestOffsetMinX,yd-DestOffsetMinY);
			D=DPOS(xd, yd);
			Xn = Xrot(xd-DestOffsetMinX, yd-DestOffsetMinY);								// Matrix rotation, zooming and translation
			Yn = Yrot(xd-DestOffsetMinX, yd-DestOffsetMinY);
			if (BETWEEN(0,Xn,Width-1) and BETWEEN(0,Yn,Height-1)) {	// Verify that the point is still within the window
				Within[i]++;
				StackNb[D*8/PixDepth]++;
				Pn=POS(Xn, Yn);
				StackSum[D] += DestBits[D] = Bits[Pn];
				if (Bits[Pn]>StackMax[D]) StackMax[D]=Bits[Pn];
				if (Bits[Pn]<StackMin[D]) StackMin[D]=Bits[Pn];
				if (PixDepth>8) {
					StackSum[D+1] += DestBits[D+1] = Bits[Pn+1];
					StackSum[D+2] += DestBits[D+2] = Bits[Pn+2];
					if (Bits[Pn+1]>StackMax[D+1]) StackMax[D+1]=Bits[Pn+1];
					if (Bits[Pn+2]>StackMax[D+2]) StackMax[D+2]=Bits[Pn+2];
					if (Bits[Pn+1]<StackMin[D+1]) StackMin[D+1]=Bits[Pn+1];
					if (Bits[Pn+2]<StackMin[D+2]) StackMin[D+2]=Bits[Pn+2];
					if (PixDepth==32) {
						StackSum[D+3] += DestBits[D+3] = Bits[Pn+3];
						if (Bits[Pn+3]>StackMax[D+3]) StackMax[D+3]=Bits[Pn+3];
						if (Bits[Pn+3]<StackMin[D+3]) StackMin[D+3]=Bits[Pn+3];
					}
				}
			} else { 						// Black missing point
				DestBits[D] = 0;
				if (PixDepth>8) {
					DestBits[D+1]  =  DestBits[D+2] = 0;
					if (PixDepth==32) DestBits[D+3] = 0;
				}
			}
		}
//	NewBitmap (RowBytes, PixDepth, Width, Height, NULL, DestBits[i], NULL, &DestBitmap[i]);
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Select the left alignement point on image i
/// HIPAR	i/The image index
///////////////////////////////////////////////////////////////////////////////
static void SetLeft(int i, int X, int Y) {
	if (NbFiles<=0) return;
	XLeft[i]=X;
	YLeft[i]=Y;
	ManualCrossL[i]=TRUE;
	DisplayImage(i); 
	if (i==0 and XRight[0]!=0) {
		MessagePopup("Select References", "Now do the same on the 2nd image:\n"
											"select the same two reference stars\n"
											"with the left and right click");
		DisplayImage(Current=1); 	// Go to next image
	} else if (i==1 and XRight[1]!=0 and XLeft[0]!=0 and XRight[0]!=0) {
		SetCtrlAttribute(Pnl, PNL_STACK, ATTR_VISIBLE, TRUE); 	// Go to Processing
		if (ConfirmPopup("Find other points automatically ?",
						 "[Yes] to try following the same stars automatically on the next images\n"
						 "[No] to do it manually"))
			Follow(1);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Select the right alignement point on image i
/// HIPAR	i/The image index
///////////////////////////////////////////////////////////////////////////////
static void SetRight(int i, int X, int Y) {
	if (NbFiles<=0) return;
	XRight[i]=X;
	YRight[i]=Y;
	ManualCrossR[i]=TRUE;
	DisplayImage(i); 
	if (i==0 and XLeft[0]!=0) {
		MessagePopup("Select References", "Now do the same on the 2nd image:\n"
											"select the same two reference stars\n"
											"with the left and right click");
		DisplayImage(Current=1); 	// Go to next image
	} else if (i==1 and XLeft[1]!=0 and XLeft[0]!=0 and XRight[0]!=0) {
		SetCtrlAttribute(Pnl, PNL_STACK, ATTR_VISIBLE, TRUE); 	// Go to Processing
		if (ConfirmPopup("Find other alignment points automatically ?",
						 "[Yes] to try following the same stars automatically on the next images\n"
						 "[No]  to do it manually.\n"
						 "You can always adjust them later."))
			Follow(1);
	}
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Canvas (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int X, Y;
	Current=FORCEPIN(0,Current,NbFiles-1);
	if (event==EVENT_LEFT_CLICK or event==EVENT_RIGHT_CLICK) {
		X =  Width*(eventData2-CanvasLeft)/CanvasWidth;
		Y = Height*(eventData1-CanvasTop )/CanvasHeight;
		FindCenter(X, Y, &X, &Y);

		switch (event) {
			case EVENT_LEFT_CLICK: SetLeft (Current, X, Y); break;
			case EVENT_RIGHT_CLICK:SetRight(Current, X, Y); break;
		}
		SetActiveCtrl (Pnl, PNL_CANVAS);	// Why ???
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Open (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	
	switch (event) {
		case EVENT_COMMIT:
			Cleanup();
			Dropped=FALSE;

			MakePathname (DriveName, DirName, Directory);
			//sprintf(Directory, "%s%s", DriveName, DirName);
			if (MultiFileSelectPopup (Directory, "*.jpg;*.jpeg;*.png",
					   "*.jpg;*.jpeg;*.png", "Select images to load and stack", 0,
					   0, 1, &NbFiles, &FileList) != VAL_EXISTING_FILE_SELECTED) return 0;

			SetWaitCursor(TRUE);
			qsort (FileList, NbFiles, sizeof(char*), STRCMP);
			
			Define();
			SetWaitCursor(FALSE);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Prepare list of all file names to write
///////////////////////////////////////////////////////////////////////////////
static void CreateFileNames(void) {
	int Lidx=0; // Index from Left
	int Ridx=0; // Index from Right
	char CharL=0, CharR=0, *DarkLabel;	// Reference char
	BOOL SearchL=TRUE, SearchR=TRUE;
	
	int i;
	char *P, S[MAX_PATHNAME_LEN]="";

	// Determine the common part to all file names (the begining and the end)
	// This is used to eliminate the numbers		
	while (SearchL or SearchR) {
		CharL=FileList[0][Lidx];	// Reference char
		CharR=FileList[0][strlen(FileList[0])-1-Ridx];	// Reference char
		for (i=0; i<NbFiles; i++) {
			if (SearchL) SearchL = ( CharL==FileList[i][Lidx] );
			if (SearchR) SearchR = ( CharR==FileList[i][strlen(FileList[i])-Ridx-1] );
		}
		if (SearchL) Lidx++;
		if (SearchR) Ridx++;
	}
	strncpy(LeftPathName, FileList[0], Lidx-1);
	LeftPathName[Lidx-1]='\0';
	strcpy (RightPathName, &FileList[0][strlen(FileList[0])-Ridx]);

	DarkLabel= (AutoDark ? "_AutoDark" : DarkFile[0]!='\0' ? "_Dark" : "");
	sprintf(StatFilePath,   "%s.txt", LeftPathName);
	sprintf(RGBMeanFilePath,"%s_RGB_Mean%s.png", LeftPathName, DarkLabel);
	sprintf(RGBMaxFilePath, "%s_RGB_Max%s.png",  LeftPathName, DarkLabel);
	sprintf(RGBMinFilePath, "%s_RGB_Min%s.png",  LeftPathName, DarkLabel);
	sprintf(RGBSortFilePath,"%s_RGB_Sort%s.png", LeftPathName, DarkLabel);
	sprintf(HSLMeanFilePath,"%s_HSL_Mean%s.png", LeftPathName, DarkLabel);
	sprintf(HSLMaxFilePath, "%s_HSL_Max%s.png",  LeftPathName, DarkLabel);
	if (AutoDark) 
		sprintf(DarkFile,   "%s_AutoDark.png",LeftPathName);
	
	// Now create the destination filenames
//	ClearListCtrl (Pnl, PNL_IMAGE_LIST);
	for (i=0; i<NbFiles; i++) {
		P=strrchr (FileList[i], '.');
		strncpy (S, FileList[i], P-FileList[i]);
		S[P-FileList[i]]='\0';
		DestFileList[i] = malloc(MAX_PATHNAME_LEN);
		sprintf(DestFileList[i], "%s_Aligned.png", S);
//		SplitPath (DestFileList[i], NULL, NULL, File);
//		InsertListItem (Pnl, PNL_IMAGE_LIST, -1, File, i);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Compare function used by quicksort algorithm to sort out intensity in order
///////////////////////////////////////////////////////////////////////////////
typedef struct sInt {
	int Pos;	
	double Int;
//	double IntR, IntG, IntB, IntA;
} tInt;

static int CompareInt(const void *element1, const void *element2) {
	return (((tInt*)element1)->Int > ((tInt*)element2)->Int) ?  1 :
		   (((tInt*)element1)->Int < ((tInt*)element2)->Int) ? -1 : 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Stack all the images
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Stack (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Progress, i, xd, yd, D, S;
	int MaxMeanR=0, MaxMeanG=0, MaxMeanB=0, MaxMeanA=0;
	int MinMeanR=0x7FFFFFFF, MinMeanG=0x7FFFFFFF, MinMeanB=0x7FFFFFFF, MinMeanA=0x7FFFFFFF;
	tInt *SortR=NULL, *SortG=NULL, *SortB=NULL, *SortA=NULL;
//	tInt *Sort=NULL;
	double CoefR, CoefG, CoefB, CoefA, Z, CurrentSumR=0, CurrentSumG=0, CurrentSumB=0, CurrentSumA=0;
	int IdxLowR=0, IdxHighR=0, IdxLowG=0, IdxHighG=0, IdxLowB=0, IdxHighB=0, IdxLowA=0, IdxHighA=0;
	
	switch (event) {
		case EVENT_COMMIT:
			if (NbFiles<=0) break;

			Progress = CreateProgressDialog ("Computing transformation parameters", "Percent Complete", 1, VAL_FULL_MARKERS, "__Cancel");
#define REPROG(Msg)	DiscardProgressDialog(Progress); Progress=CreateProgressDialog(Msg, "Percent Complete", 1, VAL_FULL_MARKERS, "__Cancel");

			CreateFileNames();
			
			// Text file with transformation data
			StatFile = fopen (StatFilePath, "w");
			if (StatFile==NULL) MessagePopup("Warning !", "Could not open statistics file");
			fprintf(StatFile, "Original File, XL, YL, XR, YR, Radius (px), Angle (�), Coverage (%%), Xmin, Ymin, Xmax, Ymax\n");

			DestOffsetMinX=DestOffsetMinY=DestOffsetMaxX=DestOffsetMaxY=0;

			// Compute transformation parameters
			for (i=0; i<NbFiles; i++) {
				ComputeTransform(i);
				if (DestOffsetMinX>XoffsetMin[i]) DestOffsetMinX=XoffsetMin[i];	// is normally negative
				if (DestOffsetMinY>YoffsetMin[i]) DestOffsetMinY=YoffsetMin[i];
				if (DestOffsetMaxX<XoffsetMax[i]) DestOffsetMaxX=XoffsetMax[i];	// is normally positive
				if (DestOffsetMaxY<YoffsetMax[i]) DestOffsetMaxY=YoffsetMax[i];
				fprintf(StatFile, "%s, %d, %d, %d, %d, %.1f, %.2f, %d, %d, %d, %d, %d\n", 
					FileList[i], XLeft[i], YLeft[i], XRight[i], YRight[i], 
					Rad[i], RadToDeg(Alpha[i]), 100*Within[i]/(Height*Width),
					XoffsetMin[i], YoffsetMin[i], XoffsetMax[i], YoffsetMax[i]);
				if (i>1 and // If no image has any cross, then fine, go ahead without any rotation
					(XLeft[0]!=0 or XRight[0]!=0 or YLeft[0]!=0 or YRight[0]!=0 or  // if one of the first two images has a cross
					 XLeft[1]!=0 or XRight[1]!=0 or YLeft[1]!=0 or YRight[1]!=0) and// and one of the others misses one
					(XLeft[i]==0 or XRight[i]==0 or YLeft[i]==0 or YRight[i]==0)) {	// Then it's probably not what we want
						sprintf(Str, "Warning, image %d:%s\nis missing an alignement point", i, FileList[i]);
						MessagePopup("Warning!", Str);
				}
			}
			fclose(StatFile); StatFile=NULL;

			///////////////////////////////////////////////////////////////////
			// Compute destination dimensions
			DestOffsetMinX=-DestOffsetMinX;			// Want positive value
			DestOffsetMinY=-DestOffsetMinY; 
			DestOffsetMaxX=DestOffsetMaxX-Width;	// Extra border dimension
			DestOffsetMaxY=DestOffsetMaxY-Height;   // Should be the same as DestOffsetMinY

			DestWidth=Width+DestOffsetMinX+DestOffsetMaxX/*+1*/;
			DestHeight=Height+DestOffsetMinY+DestOffsetMaxY/*+1*/;
			DestRowBytes=DestWidth*PixDepth/8;
			

			///////////////////////////////////////////////////////////////////
			// Memory allocations
			StackSum =calloc(DestRowBytes*DestHeight, sizeof(int));
			StackNb  =calloc(DestRowBytes*DestHeight*8/PixDepth, sizeof(int));
			StackMax =malloc(DestRowBytes*DestHeight);
			StackMin =malloc(DestRowBytes*DestHeight); memset (StackMin, 255, DestRowBytes*DestHeight);
			StackMean=malloc(DestRowBytes*DestHeight);
			StackSort=malloc(DestRowBytes*DestHeight);
			if (StackSum==NULL or StackNb==NULL or StackMax==NULL or StackMin==NULL or StackMean==NULL or StackSort==NULL) {
				MessagePopup("Error !", "Out of memory, increase your pagefile or purchase more RAM !!!");
				exit(2);
			}

			///////////////////////////////////////////////////////////////////
			REPROG("Auto-dark computation");
			
			if (AutoDark) {
				if (DarkBits!=NULL) free(DarkBits); DarkBits=NULL;
				DarkBits=malloc(PixDepth/8*RowBytes*Height);
				if (DarkBits==NULL) { MessagePopup("Error!", "Out of memory."); exit(2); }
				memset(DarkBits, 0xFF, PixDepth/8*RowBytes*Height);

				for (i=0; i<NbFiles; i++) {
					int x, y;
					unsigned char *Col, *Dark;
					if (UpdateProgressDialog (Progress, 5*i/NbFiles, 1)) goto Abort;
					ReadFile(i);
					for (y=0; y<Height; y++) {
						Col =&Bits    [y*RowBytes];
						Dark=&DarkBits[y*RowBytes];
						for (x=0; x<Width; x++) {
							Col += (PixDepth>>3);
							Dark+= (PixDepth>>3);	// Keep only the darkest pixel of the stack
							if (                Dark[0]>Col[0]) Dark[0]=Col[0];
							if (PixDepth>8  and Dark[1]>Col[1]) Dark[1]=Col[1];
							if (PixDepth>16 and Dark[2]>Col[2]) Dark[2]=Col[2];
							if (PixDepth>24 and Dark[3]>Col[3]) Dark[3]=Col[3];
						}
					}
				}
				Png_SetParameters (0.45455, 0, 28, Z_DEFAULT_COMPRESSION, PixDepth==32);
				Png_SaveBitmapToFile(DarkBits, RowBytes, PixDepth, Width, Height, DarkFile);
				DarkFile[0]='\0';
			}

			///////////////////////////////////////////////////////////////////
			// Rotate and save each image
			REPROG("Rotating images");
			for (i=0; i<NbFiles; i++) {
				if (UpdateProgressDialog (Progress, 5+15*i/NbFiles, 1)) goto Abort;
				DisplayImage(Current=i);
				PerformTransform(i);
				Png_SetParameters (0.45455, 0, 28, Z_DEFAULT_COMPRESSION, PixDepth==32);
				if (SaveRot and (!Static or AutoDark or DarkFile[0]!='\0'))
					if (DestBits!=NULL and Png_SaveBitmapToFile(DestBits, DestRowBytes, PixDepth, DestWidth, DestHeight, DestFileList[i]))
						MessagePopup("Error !", "Could not write aligned file");
			}
			
			Transformed=TRUE;
			
			///////////////////////////////////////////////////////////////////
			// Find intensity Min/Max
			REPROG("Finding intensity extrema");
			for (yd=0; yd<DestHeight; yd++) {
				if (UpdateProgressDialog (Progress, 20+20*yd/DestHeight, 1)) goto Abort;
				for (xd=0; xd<DestWidth; xd++) {
					D=DPOS(xd,yd);
					if ((S=StackNb[D*8/PixDepth])==0) continue;
					if (MaxMeanR<StackSum[D]/S) MaxMeanR=StackSum[D]/S;
					if (MinMeanR>StackSum[D]/S) MinMeanR=StackSum[D]/S;
					if (PixDepth>8) {
						if (MaxMeanG<StackSum[D+1]/S) MaxMeanG=StackSum[D+1]/S;
						if (MaxMeanB<StackSum[D+2]/S) MaxMeanB=StackSum[D+2]/S;
						if (MinMeanG>StackSum[D+1]/S) MinMeanG=StackSum[D+1]/S;
						if (MinMeanB>StackSum[D+2]/S) MinMeanB=StackSum[D+2]/S;
						if (PixDepth==32) {
							if (MaxMeanA<StackSum[D+3]/S) MaxMeanA=StackSum[D+3]/S;
							if (MinMeanA>StackSum[D+3]/S) MinMeanA=StackSum[D+3]/S;
						}
					}
				}
			}


			///////////////////////////////////////////////////////////////////
			// Allocate sort arrays
			SortR=calloc(DestWidth*DestHeight, sizeof(tInt));
			if (SortR==NULL) { MessagePopup("Error !", "Out of memory, increase your page file"); exit(2); }
			if (PixDepth>8) {
				SortG=calloc(DestWidth*DestHeight, sizeof(tInt));
				SortB=calloc(DestWidth*DestHeight, sizeof(tInt));
				if (SortG==NULL or SortB==NULL) { MessagePopup("Error !", "Out of memory, increase your page file"); exit(2); }
				if (PixDepth==32) {
					SortA=calloc(DestWidth*DestHeight, sizeof(tInt));
					if (SortA==NULL) { MessagePopup("Error !", "Out of memory, increase your page file"); exit(2); }
				}
			}
			
			///////////////////////////////////////////////////////////////////
			SumR=SumG=SumB=SumA=0,
			SSmR=SSmG=SSmB=SSmA=0,
			CntR=CntG=CntB=CntA=0;
			
			///////////////////////////////////////////////////////////////////
			// Normalize, compute histogram and fill sort arrays
			REPROG("Normalization, histogram computation");
			for (yd=0; yd<DestHeight; yd++) {
				if (UpdateProgressDialog (Progress, 40+20*yd/DestHeight, 1)) goto Abort;
				for (xd=0; xd<DestWidth; xd++) {
					D = DPOS(xd,yd);
					if ((S=StackNb[D*8/PixDepth])==0) 
						continue;
					StackMean[SortR[xd+yd*DestWidth].Pos = D] = 
						255*((Z = SortR[xd+yd*DestWidth].Int = (double)StackSum[D]/S)-MinMeanR)/(MaxMeanR-MinMeanR);
					SumR+=Z; SSmR+=Z*Z; CntR++;
					if (PixDepth>8) {
						StackMean[SortG[xd+yd*DestWidth].Pos = D+1] = 
							255*((Z = SortG[xd+yd*DestWidth].Int = (double)StackSum[D+1]/S)-MinMeanG)/(MaxMeanG-MinMeanG);
						SumG+=Z; SSmG+=Z*Z; CntG++;
						StackMean[SortB[xd+yd*DestWidth].Pos = D+2] = 
							255*((Z = SortB[xd+yd*DestWidth].Int = (double)StackSum[D+2]/S)-MinMeanB)/(MaxMeanB-MinMeanB);
						SumB+=Z; SSmB+=Z*Z; CntB++;
						if (PixDepth==32) {
							StackMean[SortA[xd+yd*DestWidth].Pos = D+3] = 
								255*((Z = SortA[xd+yd*DestWidth].Int = (double)StackSum[D+3]/S)-MinMeanA)/(MaxMeanA-MinMeanA);
							SumA+=Z; SSmA+=Z*Z; CntA++;
						}
					}
				}
			}			
			

			///////////////////////////////////////////////////////////////////
			// Sort the intensities
			REPROG("Array sorts");
			if (UpdateProgressDialog (Progress, 65, 1)) goto Abort;
			qsort (SortR, DestHeight*DestWidth, sizeof(tInt), CompareInt);
			
			if (PixDepth>8) {
				if (UpdateProgressDialog (Progress, 70, 1)) goto Abort;
				qsort (SortG, DestHeight*DestWidth, sizeof(tInt), CompareInt);
				
				if (UpdateProgressDialog (Progress, 75, 1)) goto Abort;
				qsort (SortB, DestHeight*DestWidth, sizeof(tInt), CompareInt);
				
				if (PixDepth==32) {
					if (UpdateProgressDialog (Progress, 80, 1)) goto Abort;
					qsort (SortA, DestHeight*DestWidth, sizeof(tInt), CompareInt);
				}
			}

			///////////////////////////////////////////////////////////////////
			#define AtoB(A,B) (exp((B)*log(A)))
			
			// Find Thresholds
			for (i=0; i<DestHeight*DestWidth; i++) {
				CurrentSumR+=SortR[i].Int;
				if (CurrentSumR/SumR < ThresholdLow /100)  IdxLowR=i;
				if (CurrentSumR/SumR < ThresholdHigh/100) IdxHighR=i;

				if (PixDepth>8) {
					CurrentSumG+=SortG[i].Int;
					if (CurrentSumG/SumG < ThresholdLow /100)  IdxLowG=i;
					if (CurrentSumG/SumG < ThresholdHigh/100) IdxHighG=i;

					CurrentSumB+=SortB[i].Int;
					if (CurrentSumB/SumB < ThresholdLow /100)  IdxLowB=i;
					if (CurrentSumB/SumB < ThresholdHigh/100) IdxHighB=i;
				
					if (PixDepth==32) {
						CurrentSumA+=SortA[i].Int;
						if (CurrentSumA/SumA < ThresholdLow /100)  IdxLowA=i;
						if (CurrentSumA/SumA < ThresholdHigh/100) IdxHighA=i;
					}
				}
			}

			///////////////////////////////////////////////////////////////////
			// Reorder the intensities using a linear (or gamma) scale instead of the sorted values
			REPROG("Intensity reordering");
			for (i=0; i<DestHeight*DestWidth; i++) {
				if (i%DestWidth==0 and UpdateProgressDialog (Progress, 80+20*i/(DestHeight*DestWidth), 1)) goto Abort;
				
				if      (i < IdxLowR)  CoefR=0;
				else if (i > IdxHighR) CoefR=1;
				else {
					CoefR=((double)i-IdxLowR)/(IdxHighR-IdxLowR);
					if (0<CoefR and CoefR<1)
						CoefR=AtoB(1-AtoB(1-CoefR,1/Gamma), Gamma);
				}
				StackSort[SortR[i].Pos] = FORCEPIN(0, 255*CoefR, 255);
//				
//				printf("%d %f %f %f %f %dx%d %dx%d %dx%d\n", i, Coef, 
//					SortR[i].Int, SortG[i].Int, SortB[i].Int, 
//					SortR[i].Pos/RowBytes, SortR[i].Pos%RowBytes/3,
//					SortG[i].Pos/RowBytes, (SortG[i].Pos-1)%RowBytes/3,
//					SortB[i].Pos/RowBytes, (SortB[i].Pos-2)%RowBytes/3);
				

				if (PixDepth>8) {
					if (i < IdxLowG) CoefG=0;
					else if (i > IdxHighG) CoefG=1;
					else {
						CoefG=((double)i-IdxLowG)/(IdxHighG-IdxLowG);	// expand
						if (0<CoefG and CoefG<1)
							CoefG=AtoB(1-AtoB(1-CoefG,1/Gamma), Gamma);
					}
					StackSort[SortG[i].Pos] = FORCEPIN(0, 255*CoefG, 255);
					
					if (i < IdxLowB) CoefB=0;
					else if (i > IdxHighB) CoefB=1;
					else {
						CoefB=((double)i-IdxLowB)/(IdxHighB-IdxLowB);
						if (0<CoefB and CoefB<1)
							CoefB=AtoB(1-AtoB(1-CoefB,1/Gamma), Gamma);
					}
					StackSort[SortB[i].Pos] = FORCEPIN(0, 255*CoefB, 255);
					if (PixDepth==32) {
						if (i < IdxLowA) CoefA=0;
						else if (i > IdxHighA) CoefA=1;
						else {
							CoefA=((double)i-IdxLowA)/(IdxHighA-IdxLowA);
							if (0<CoefA and CoefA<1)
								CoefA=AtoB(1-AtoB(1-CoefA,1/Gamma), Gamma);
						}
						StackSort[SortA[i].Pos] = FORCEPIN(0, 255*CoefA, 255);
					}
				}
			}

			REPROG("Saving stacked images");
			if (UpdateProgressDialog (Progress, 100, 1)) goto Abort;

			///////////////////////////////////////////////////////////////////
			// Save files
			Png_SetParameters (0.45455, 0, 28, Z_DEFAULT_COMPRESSION, PixDepth==32);
			if (Png_SaveBitmapToFile (StackMax,  DestRowBytes, PixDepth, DestWidth, DestHeight, RGBMaxFilePath)  or
				Png_SaveBitmapToFile (StackMean, DestRowBytes, PixDepth, DestWidth, DestHeight, RGBMeanFilePath) or
				Png_SaveBitmapToFile (StackMin,  DestRowBytes, PixDepth, DestWidth, DestHeight, RGBMinFilePath)  or
				Png_SaveBitmapToFile (StackSort, DestRowBytes, PixDepth, DestWidth, DestHeight, RGBSortFilePath)
			) MessagePopup("Error !", "Cannot save result files");
				
Abort:		DiscardProgressDialog (Progress);
			
			///////////////////////////////////////////////////////////////////
			// Free memory
			free(SortR); SortR=NULL;
			if (PixDepth>8) {
				free(SortG); SortG=NULL;
				free(SortB); SortB=NULL;
				if (PixDepth==32) free(SortA); SortA=NULL;
			}
			free(StackSum); StackSum=NULL;
			free(StackNb);  StackNb=NULL;
			free(StackMax); StackMax=NULL;
			free(StackMin); StackMin=NULL;
			free(StackMean);StackMean=NULL;
			free(StackSort);StackSort=NULL;


			MessagePopup("Done !", "The rotation and stacking process is finished\n"
						"The aligned imaged as well as the stacked results\n"
						"have been saved in the same directory.\n"
						"You can now quit, start with another set or\n"
						"adjust the reference points to redo the stacking.");
			
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Stack", "Press this button to align all the images, stack them\n"
							"and save various result files in the same directory\n"
							"than the original images.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Animate (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	static BOOL Animate=FALSE;

	switch (event) {
		case EVENT_COMMIT:
			if (NbFiles<=0) break;
			GetCtrlVal(panel, control, &Animate);
			while (Animate) {
				Current=(Current+1)%NbFiles;
				DisplayImage(Current);
				ProcessDrawEvents ();
				ProcessSystemEvents ();
			}
			break;
		case EVENT_LOST_FOCUS:
			SetCtrlVal(panel, control, Animate=FALSE);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Animate", "Click this to animate the various images\n"
						"The manually set reference points have a vertical cross,\n"
						"while the computed points have a diagonal cross.\n"
						"The Point set with the left click is yellow and the right click in cyan.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_11 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int CanvasTop, CanvasLeft, ScreenWidth, ScreenHeight, Z;
	switch (event) {
		case EVENT_COMMIT:
			if (NbFiles<=0) return 0;
			GetScreenSize (&ScreenHeight, &ScreenWidth);
			Z=MAX3(1, 1+Width/ScreenWidth, 1+Height/ScreenHeight);
			SetCtrlAttribute (panel, PNL_CANVAS, ATTR_WIDTH,  CanvasWidth = Width/Z);
			SetCtrlAttribute (panel, PNL_CANVAS, ATTR_HEIGHT, CanvasHeight=Height/Z);
			GetCtrlAttribute (panel, PNL_CANVAS, ATTR_TOP,  &CanvasTop);
			GetCtrlAttribute (panel, PNL_CANVAS, ATTR_LEFT, &CanvasLeft);
			SetPanelAttribute(panel, ATTR_WIDTH, CanvasWidth+CanvasLeft);
			SetPanelAttribute(panel, ATTR_HEIGHT,CanvasHeight+CanvasTop);
			DisplayImage(Current);

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help 1:n", "Set the window size to 1:1 scale (if it fits, otherwise 1:2, 1:3...)");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Show/Hide the advanced controls
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Advanced (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Adv;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Adv);
			SetCtrlAttribute(Pnl, PNL_SEARCH,			ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_BLUR,				ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_GAMMA,			ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_THRESHOLD_LOW,	ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_THRESHOLD_HIGH,	ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_NEGATIVE,			ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_SAVE_ROT,			ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_COLOR_LEFT,		ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_COLOR_RIGHT,		ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_USE_DARK_FILE,	ATTR_VISIBLE, Adv);
			SetCtrlAttribute(Pnl, PNL_AUTO_DARK,		ATTR_VISIBLE, Adv);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Advanced", "Show a few extra parameters");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Negative (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Negative);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Negative", "When clicking on a point on the image,"
						"\nlook for darkest point if selected,"
						"\notherwise look for lightest point (i.e. star)"
						"\nin a SearchWidth*SearchWidth window around the point you clicked");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Colors (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, control==PNL_COLOR_LEFT ? &ColorLeft : &ColorRight);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Colors", "Colors of the left and right centering arrows.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_SaveRot (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &SaveRot);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Save Rotated", "Save the rotated images to separate PNG files.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_AutoDark (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &AutoDark);
			SetCtrlAttribute(panel, PNL_USE_DARK_FILE, ATTR_DIMMED, AutoDark);
			SetCtrlVal(panel, PNL_USE_DARK_FILE, FALSE);
			cb_UseDark (panel, PNL_USE_DARK_FILE, EVENT_COMMIT, NULL, 0, 0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help Auto-generated dark file", 
				"In some cases it may be possible to extrapolate the dark file by doing the following:\n"
				"Stack all the images without rotation and for each of the x/y pixels,\n"
				"take the darkest of the stack.\n\n"
				"For this method to work the images must be fairly dark already,\n"
				"and there shouldn't be bright areas common to all the images.\n"
				"The resulting dark file will be saved along with the results.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_UseDark (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	BOOL UseDark;
	char FileName[MAX_FILENAME_LEN], Str[255];
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &UseDark);
			if (UseDark) {
				MakePathname (DriveName, DirName, Directory);
				if (VAL_EXISTING_FILE_SELECTED!=FileSelectPopup (Directory, "*.jpg;*.jpeg;*.png", 
					"*.jpg;*.jpeg;*.png", "Select DARK file", VAL_SELECT_BUTTON, 
					0, 1, 1, 0, DarkFile)) DarkFile[0]='\0';
				else {
					if (DarkBits!=NULL) free(DarkBits); DarkBits=NULL;
	
					if (IsJpeg(DarkFile)) {
						if (Jpg_ReadBitmapFromFile (NULL, &DarkBits, &RowBytes, &PixDepth, &Width, &Height, DarkFile)) {
							sprintf(Str, "Error reading file %s", DarkFile);
							MessagePopup("Error!", Str); DarkFile[0]='\0';
							goto Fail;
						}
					} else if (IsPng(DarkFile)) {
						if (Png_ReadBitmapFromFile (NULL, &DarkBits, &RowBytes, &PixDepth, &Width, &Height, DarkFile)) {
							sprintf(Str, "Error reading file %s", DarkFile);
							MessagePopup("Error!", Str); DarkFile[0]='\0';
							goto Fail;
						}
					} else {
						sprintf(Str, "File %s is not png or jpg", DarkFile);
						MessagePopup("Error!", Str); DarkFile[0]='\0';
						goto Fail;
					}
					SplitPath (DarkFile, NULL, NULL, FileName);
					sprintf(Str, "Use a 'Dark' file: %s", FileName);
					SetCtrlAttribute(Pnl, PNL_USE_DARK_FILE, ATTR_LABEL_TEXT, Str);
					SetCtrlAttribute(Pnl, PNL_AUTO_DARK, ATTR_DIMMED, TRUE);
					SetCtrlVal(Pnl, PNL_AUTO_DARK, AutoDark=FALSE);
				}
			} else DarkFile[0]='\0';

			if (strlen(DarkFile)==0) {
Fail:			SetCtrlAttribute(Pnl, PNL_USE_DARK_FILE, ATTR_LABEL_TEXT, "Use a 'Dark' file");
				SetCtrlVal      (Pnl, PNL_USE_DARK_FILE, FALSE);
				SetCtrlAttribute(Pnl, PNL_AUTO_DARK, ATTR_DIMMED, FALSE);
			}
			break;

		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Dark file", 
				"A dark file is an image taken with the same exposure time as your regular images,\n"
				"but with the cover on the lens, resulting in what _should_ be a completely dark image.\n"
				"This 'dark' image contains precious information about sensor defects such as\n"
				"hot pixels and average noise level.\n"
				"By substracting this dark frame from each input image\n"
				"it's possible to increase the quality of the result.\n"
				"But you must remember to take a dark shot while you do your exposures!\n"
				"You can't take a dark later as the temperature of the sensor is an important factor\n"
				"and all other settings should be the same.\n"
				"The DARK file must have the same dimensions as the other images.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Follow (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			Follow(Current);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Follow alignment points", 
				"From the current and previous image,\n"
				"if alignment points have been selected,\n"
				"generate the alignement points for all the remaining images");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_SearchPixels (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &SearchWidth);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Search area", "This is the dimensions in pixels of the window\n"
						"on which to search for the intensity peak.\n"
						"If the search finds the wrong 'peak', you can always correct it manually\n"
						"with a left or right-click.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Blur (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Blur);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Blur area", "This is the dimensions in pixels of the bluring\n"
						"of the pixels when searching for the intensity peak.\n"
						"[NOT CURRENTLY USED]");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Gamma (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Gamma);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Gamma", "This is the gamma slope of the histogram sorting.\n"
						"Use 1 for no gamma, default should be around 1.5.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_ThresholdLow (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &ThresholdLow);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Threshold", "During the reordering of the histogram,\n"
						"The percentage of pixels under this value are set to black (=0).\n"
						"Default is 1%.\n"
						"A value set too low can create artifacts.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_ThresholdHigh (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &ThresholdHigh);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Threshold", "During the reordering of the histogram,\n"
						"The percentage of pixels over this value are set to white.\n"
						"Default should be 96 to 99%.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_ImageList (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_VAL_CHANGED:
			GetCtrlIndex (Pnl, PNL_IMAGE_LIST, &Current);
			DisplayImage(Current);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: File list", "List of image file names loaded in memory.\n"
					"The list is sorted alphabetically, so make sure you don't have file names\n"
					"like File1.jpg, File2.kpg... Files10.jpg but\n"
					"File01.jpg, File02.jpg... File10.jpg instead.\n\n"
					"You can use the [Up] and [Down] keyboard arrows to go from one image to the next.");
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:

			break;
	}
	return 0;
}

