/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2011. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNL                              1       /* callback function: cbp_Panel */
#define  PNL_OPEN                         2       /* control type: command, callback function: cb_Open */
#define  PNL_11                           3       /* control type: command, callback function: cb_11 */
#define  PNL_IMAGE_LIST                   4       /* control type: ring, callback function: cb_ImageList */
#define  PNL_ADVANCED                     5       /* control type: textButton, callback function: cb_Advanced */
#define  PNL_SEARCH                       6       /* control type: numeric, callback function: cb_SearchPixels */
#define  PNL_BLUR                         7       /* control type: numeric, callback function: cb_Blur */
#define  PNL_GAMMA                        8       /* control type: numeric, callback function: cb_Gamma */
#define  PNL_THRESHOLD_LOW                9       /* control type: numeric, callback function: cb_ThresholdLow */
#define  PNL_THRESHOLD_HIGH               10      /* control type: numeric, callback function: cb_ThresholdHigh */
#define  PNL_AUTO_DARK                    11      /* control type: radioButton, callback function: cb_AutoDark */
#define  PNL_USE_DARK_FILE                12      /* control type: radioButton, callback function: cb_UseDark */
#define  PNL_NEGATIVE                     13      /* control type: radioButton, callback function: cb_Negative */
#define  PNL_SAVE_ROT                     14      /* control type: radioButton, callback function: cb_SaveRot */
#define  PNL_COLOR_LEFT                   15      /* control type: color, callback function: cb_Colors */
#define  PNL_COLOR_RIGHT                  16      /* control type: color, callback function: cb_Colors */
#define  PNL_ANIMATE                      17      /* control type: textButton, callback function: cb_Animate */
#define  PNL_FOLLOW                       18      /* control type: command, callback function: cb_Follow */
#define  PNL_STACK                        19      /* control type: command, callback function: cb_Stack */
#define  PNL_QUIT                         20      /* control type: command, callback function: cb_Quit */
#define  PNL_CANVAS                       21      /* control type: canvas, callback function: cb_Canvas */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK cb_11(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Advanced(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Animate(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_AutoDark(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Blur(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Canvas(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Colors(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Follow(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Gamma(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ImageList(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Negative(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Open(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_SaveRot(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_SearchPixels(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Stack(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ThresholdHigh(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ThresholdLow(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_UseDark(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cbp_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif


