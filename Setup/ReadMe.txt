///////////////////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   RotAndStack.exe                                                        
//
// VERSION:   1.1
//
// COPYRIGHT: � 2005 Guillaume Dargaud - Freeware                                 
//
// PURPOSE:   Rot'n'Stack is a basic astrophography freeware.
//            It takes a bunch of image files (jpg or png) of same size.
//            It asks you for two reference stars on the first two images,
//            then it search for the same stars on the other images.
//            Use [Animate] or the up/down arrow keys to navigate images.
//            After you confirm the location of those stars you can
//            launch the rotation process, where all the images are aligned
//            and launch the stacking process where the images are merged
//            using various algorithms.
//            All intermediate and final images are saved into the same directory
//            as your original images.
//
// INSTALL:   run SETUP. 
//            If setup fails, you may need to install the LabWindows/CVI runtime engine 7.0 on your PC.
//            It is available from the National Instrument website http://www.ni.com/
//
// HELP:      available by right-clicking an item on the user interface.
//
// TUTORIAL:  http://www.gdargaud.net/Hack/RotAndStack.html
//
// TECH-SUPPORT: http://www.gdargaud.net/Photo/Copyright.html#Contact
//
// HISTORY:   v1.0 - 2005/07/20 - Original release
//            v1.1 - 2005/08/12 - Minor modifications and bug fixes. Expanded destination image
//
// KNOWN BUG: - If the image has very few colors, the sort algo might output banded colors.
//            - Not particularly fast, but not limited in number of images
//
// TO DO:     Save custom parameters in registry
///////////////////////////////////////////////////////////////////////////////////////////////////
